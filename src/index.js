import express from "express";
import envConfig from "dotenv/config";
import cors from "cors";
import { producer, consumer, registry } from "./kafka.js";

envConfig;
const { PORT, KAFKA_TOPIC: topic } = process.env;

const app = express();

app.use(cors());
app.use(express.json());

app.listen(PORT, () => console.log(`magic happens at ${PORT}`));

app.get("/", (req, res) => res.json({ msg: "This is home." }));
app.post("/kafka-producer", async (req, res) => {
  try {
    const valueSchemaId = await registry.getLatestSchemaId(`${topic}-value`);
    // Get latest schema id by subject, subject is passed in the argument
    // In confluent, subject === `${topic}-value` or subject === `${topic}-key`
    // depending on whether the schema is for value or for key.

    await producer.connect();
    const response = await producer.send({
      topic,
      messages: [
        // { value: req.body },
        // { value: JSON.stringify(req.body), key: req.body.key },
        {
          key: req.body.key,
          value: await registry.encode(valueSchemaId, req.body.value),
        },
      ],
    });

    await producer.disconnect();

    console.log(response);
    res.json({ msg: response });
  }catch(error){
    console.log(error);
    res.json({ error: error.message });
  }
});
app.get("/kafka-consumer", async (req, res) => {
  try {
    const { q } = req.query;
    if(parseInt(q)) {
      await consumer.disconnect();
      console.log("consumer disconnected");
      res.json({ msg: "consumer disconnected" });
      return;
    }
    await consumer.connect();
    await consumer.subscribe({ topic, fromBeginning: false });

    await consumer.run({
      eachMessage: async ({ topic, partition, message }) => {
        console.log({
          message,
          value: await registry.decode(message.value),
        })
      },
      
      // eachBatch: async ({ batch }) => {
      //   console.log(batch)
      // },
    });

    res.json({ msg: "consumer is listening, check your log." });
  }catch(error){
    console.log(error);
    res.json({ error: error.message });
  }
});
