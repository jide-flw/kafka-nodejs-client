import { Kafka, Partitioners } from "kafkajs";
import { SchemaRegistry } from "@kafkajs/confluent-schema-registry";

const {
  KAFKA_USERNAME: username,
  KAFKA_PASSWORD: password,
  KAFKA_BOOTSTRAP_SERVER: brokers,
  KAFKA_CLIENT_ID: clientId,
  KAFKA_GROUP_ID: groupId,
  KAFKA_SCHEMA_REGISTRY_URL: schemaURL,
  KAFKA_SCHEMA_USERNAME: schemaUsername,
  KAFKA_SCHEMA_PASSWORD: schemaPassword,
} = process.env;

const sasl = {
  username,
  password,
  mechanism: 'plain',
};
const ssl = !!sasl;

const registry = new SchemaRegistry({
  host: schemaURL,
  auth: {
    username: schemaUsername,
    password: schemaPassword,
  },
});

const kafka = new Kafka({
  clientId,
  brokers: [brokers],
  ssl,
  sasl,
});

const producer = kafka.producer({ createPartitioner: Partitioners.LegacyPartitioner });
const consumer = kafka.consumer({ groupId });

export {
  producer,
  consumer,
  registry,
};
