# Kafka Nodejs Client
### using [Confluent](https://confluent.cloud/)



## How to run app.
- Clone the app. <br>
`git clone https://gitlab.com/jide-flw/kafka-nodejs-client.git`
- Install the packages `npm i`
- Add environment variales.
- - create .env file in the root of the project
- - copy the content of .env.sample into your .env file
- - add values to the variables from your Confluent dashboard
- Start the app `npm start`


## Endpoints
- GET `localhost:3000/kafka-consumer`<br>
Subscribe consumer to a topic
- POST `localhost:3000/kafka-producer`
Pushes message in the body to kafka. Body should look like this:
```
{
    "value": {
        "fullName": "jide ajayi"
    },
    "key": "ju"
}
```
